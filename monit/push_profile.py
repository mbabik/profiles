import requests
import argparse
import json
import yaml


def _enrich_with_metafields(json_document):
    sitemon_document = json_document

    sitemon_document["producer"] = "sitemon"
    sitemon_document["type_prefix"] = "raw"
    sitemon_document["type"] = "profiles"

    return sitemon_document


def _generate_flavours_dictionary(flavours):
    flavours_dict = {}
    for flavour in flavours:
        flavours_dict[flavour['NAME']] = flavour
        flavours_dict[flavour['NAME']].pop('NAME')

    return flavours_dict


def _generate_profile_json(profiles_file, profile):
    profile_name = profiles_file.split('/')[-1].replace('.yaml', '').upper()
    profile_algorithm = profile['PROFILE_ALGORITHM']
    profile_flavours = profile['FLAVOURS']
    profile_vo = profile['VO']
    profile_production_flag = profile['ONLY_PRODUCTION']

    profile_dict = {
      "name": profile_name,
      "algorithm": profile_algorithm,
      "flavours": _generate_flavours_dictionary(profile_flavours),
      "vo": profile_vo,
      "only_production": profile_production_flag,
      "file": profiles_file.split('/')[-2]
    }

    return profile_dict


def _get_profiles_data(profiles_file):
    with open(profiles_file) as yaml_file:
      profile_yaml = yaml.safe_load(yaml_file)

    return _enrich_with_metafields(_generate_profile_json(profiles_file,
                                                          profile_yaml))


def _send_request(metricsource_url, json_data):
    """
    Sends a JSON dump to an HTTP endpoint

    Parameters:
    metricsource_url(string): The endpoint to send the data to
    json_data(dict): The dict representation of the data to send

    Raises:
    HTTPException: If the return code is not 200
    """
    response = requests.post(metricsource_url, data=json.dumps(json_data),
                             headers={"Content-Type": "application/json; charset=UTF-8"})
    response.raise_for_status()


def _parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='Script to push the YAML content of the profiles requests')
    parser.add_argument('--metricsource_url', type=str, required=True, help='MONIT metrics source address')
    parser.add_argument('--file', type=str, required=True, help='path to file')
    return parser.parse_args()


def main():
    args = _parse_args()
    try:
        docs = _get_profiles_data(args.file)
        response = _send_request(args.metricsource_url, docs)
        print ("exit 0")
    except Exception as ex:
        print("exit 1")
        raise ex


if __name__ == "__main__":
    main()
